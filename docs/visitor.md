<div align="center">
  <h1>Visitor</h1>
</div>

<div align="center">
  <img src="visitor_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Visitor is a behavioral pattern that lets you separate algorithms from the objects on which
they operate.**

### Real-World Analogy

_Selling specialized insurance policies per organization._

A seasoned broker is selling specialized insurance depending on the type of organization that occupies the building.
Medical insurance for residential buildings, theft insurance for banks, fire insurance for restaurants, etc.

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

Visitor (NodeVisitor)- declares a Visit operation for each class of ConcreteElement in the object structure. The
operation's name and signature identifies the class that sends the Visit request to the visitor. That lets the visitor
determine the concrete class of the element being visited. Then the visitor can access the element directly through its
particular interface. • Concrete Visitor (TypeCheckingVisitor)- implements each operation declared by Visitor. Each
operation implements a fragment of the algorithm defined for the corresponding class of object in the structure.
ConcreteVisitor provides the context for the algorithm and stores its local state. This state often accumulates results
during the traversal of the structure. • Element (Node)- defines an Accept operation that takes a visitor as an
argument.VISITOR 335 • ConcreteElement (AssignmentNode,VariableRefNode)- implements an Accept operation that takes a
visitor as an argument. • ObjectStructure (Program)- can enumerate its elements.- may provide a high-level interface to
allow the visitor to visit its elements.- may either be a composite (see Composite (163)) or a collection such as a list
or a set.

### Collaborations

...
A client that uses the Visitor pattern must create a ConcreteVisitor object and
then traverse the object structure, visiting each element with the visitor.
• When an element is visited, it calls the Visitor operation that corresponds to
its class. The element supplies itself as an argument to this operation to let the
visitor access its state, if necessary.
The following interaction diagram illustrates the collaborations between an
object structure, a visitor, and two elements:

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

• an object structure contains many classes of objects with differing interfaces, and you want to perform operations on
these objects that depend on their concrete classes. • many distinct and unrelated operations need to be performed on
objects in an object structure, and you want to avoid "polluting" their classes with these operations. Visitor lets you
keep related operations together by defining them in one class. When the object structure is shared by many
applications, use Visitor to put operations in just those applications that need them. • the classes defining the object
structure rarely change, but you often want to define new operations over the structure. Changing the object structure
classes requires redefining the interface to all visitors, which is potentially costly. If the object structure classes
change often, then it's probably better to define the operations in those classes.

### Motivation

- ...

Consider a compiler that represents programs as abstractsyntax trees. Itwill need
to perform operations on abstract syntax trees for "static semantic" analyses like
checking that all variables are defined.Itwill also need to generate code.Soit might
define operations for type-checking, code optimization, flow analysis, checking
for variables being assigned values before they're used, and so on. Moreover,
we could use the abstract syntax trees for pretty-printing, program restructuring,
code instrumentation, and computing various metrics of a program.

Most of these operations will need to treat nodes that represent assignment statements differently from nodes that
represent variables or arithmetic expressions.
Hence there will be one class for assignment statements, another for variable
accesses, another for arithmetic expressions, and so on. The set of node classes
depends on the language being compiled, of course, but it doesn't change much
for a given language

This diagram shows part of the Node class hierarchy. The problem here is that
distributing all these operations acrossthe various node classes leads to a system
that's hard to understand, maintain, and change.Itwill be confusing to have typechecking code mixed with
pretty-printing code or flow analysis code. Moreover,
adding a new operation usually requires recompiling all of these classes. It would
be better if each new operation could be added separately, and the node classes
were independent of the operations that apply to them.

We can have both by packaging related operations from each class in a separate
object, called a visitor, and passing it to elements of the abstract syntax tree as it's
traversed. When an element "accepts" the visitor, it sends a request to the visitor
that encodes the element's class. It also includes the element as an argument.The
visitor will then execute the operation for that element—the operation that used
to be in the class of the element.

For example, a compiler that didn't use visitors might type-check a procedure
by calling the TypeCheck operation on its abstract syntax tree. Each of the nodes
would implement TypeCheck by calling TypeCheck on its components (see the
preceding class diagram). Ifthe compiler type-checked a procedure using visitors,
then it would create a TypeCheckingVisitor object and call the Accept operation
on the abstract syntax tree with that object as an argument. Each of the nodes
would implement Accept by calling back on the visitor: an assignment node
calls VisitAssignment operation on the visitor, while a variable reference calls
VisitVariableReference. What used to be the TypeCheck operation in class AssignmentNode is now the VisitAssignment
operation onTypeCheckingVisitor.

Tomake visitorswork for more thanjusttype-checking,we need an abstract parent
class NodeVisitor for all visitors of an abstract syntax tree. NodeVisitor must
declare an operation for each node class. An application that needs to compute
program metrics will define new subclasses of NodeVisitor and will no longer
need to add application-specific code to the node classes. The Visitor pattern
encapsulates the operations for each compilation phase in a Visitor associated
with that phase.

---

Use the Visitor when you need to perform an operation on all elements of a complex object structure (for example, an
object tree).

The Visitor pattern lets you execute an operation over a set of objects with different classes by having a visitor
object implement several variants of the same operation, which correspond to all target classes.

Use the Visitor to clean up the business logic of auxiliary behaviors.

The pattern lets you make the primary classes of your app more focused on their main jobs by extracting all other
behaviors into a set of visitor classes.

Use the pattern when a behavior makes sense only in some classes of a class hierarchy, but not in others.

You can extract this behavior into a separate visitor class and implement only those visiting methods that accept
objects of relevant classes, leaving the rest empty.

### Known Uses

- ...

Document/AST Traversal: In compilers or interpreters, the Visitor pattern can be used to traverse abstract syntax
trees (ASTs) to perform various operations like type checking, optimization, or code generation.

Data Structure Operations: When dealing with complex data structures like trees, graphs, or composite structures, the
Visitor pattern can help perform operations such as traversal, searching, or manipulation without altering the structure
itself.

UI Components: For GUI frameworks, the Visitor pattern can be used to perform different operations on UI components. For
example, calculating the total size of elements, rendering to different output formats, or performing accessibility
checks.

Database Operations: When dealing with a database schema represented by a set of classes, the Visitor pattern can be
used to perform operations like serialization, querying, or data manipulation without directly modifying the database
classes.

Semantic Analysis: In language processing or analysis tools, the Visitor pattern can help perform semantic analysis by
visiting nodes in an abstract syntax tree and performing various checks or transformations based on the node types.

Game Development: For games with complex object hierarchies (e.g., game entities, behaviors, or levels), the Visitor
pattern can be used to apply various game mechanics or interactions without altering the existing entities.

Traversal in File Systems: When dealing with file systems, the Visitor pattern can be employed to perform operations
like calculating disk space usage, generating file statistics, or applying filters without changing the file system
structure.

Network Protocols: When dealing with networking protocols, such as TCP/IP or UDP, the Visitor pattern can be used to
traverse and manipulate different protocol headers or payloads based on the specific protocol being used.

Visitor in GUI Libraries: GUI libraries often utilize the Visitor pattern for event handling. Visitors can traverse the
GUI component hierarchy and perform actions based on different types of events (e.g., mouse clicks, keyboard input).

Financial Applications: In financial systems, the Visitor pattern can help in processing various financial instruments (
stocks, bonds, derivatives) by applying different calculations or operations without changing the instrument classes.

Medical Applications: In medical software, the Visitor pattern can be applied to analyze patient records or medical
data. It could help in performing checks, generating reports, or processing different types of medical tests.

Visitor in AI and Machine Learning: In machine learning models or AI systems, the Visitor pattern can be used to
traverse different components of the model for tasks like feature extraction, analysis, or modification without altering
the core model structure.

Serialization/Deserialization: When working with object serialization/deserialization frameworks, the Visitor pattern
can assist in the process by visiting object structures and handling the conversion to/from different formats (JSON,
XML, etc.).

Virtual Machines: In the context of virtual machines or interpreters, the Visitor pattern can aid in executing
operations on bytecode or intermediate representations of programs, such as interpretation or optimization passes.

Visitor for Code Analysis: In software development tools like static code analyzers or linters, the Visitor pattern can
be used to traverse codebases and perform analysis, detecting coding patterns, potential bugs, or enforcing coding
standards.

javax.lang.model.element.AnnotationValue and AnnotationValueVisitor
javax.lang.model.element.Element and ElementVisitor
javax.lang.model.type.TypeMirror and TypeVisitor
java.nio.file.FileVisitor and SimpleFileVisitor
javax.faces.component.visit.VisitContext and VisitCallback

The Smalltalk-80 compiler has a Visitor class called ProgramNodeEnumerator.
It's used primarily for algorithms that analyze source code. It isn't used for code
generation or pretty-printing, although it could be.
IRIS Inventor [Str931 is a toolkit for developing 3-D graphics applications. Inventor
represents a three-dimensional scene as a hierarchy of nodes, each representing
either a geometric object or an attribute of one. Operations like rendering a scene
or mapping an input event require traversing this hierarchy in different ways.
Inventor does this using visitors called "actions." There are different visitors for
rendering, event handling, searching, filing, and determining bounding boxes.
To make adding new nodes easier, Inventor implements a double-dispatch scheme
for C++. The scheme relies on run-time type information and a two-dimensional
table in which rows represent visitors and columns represent node classes. The
cells store a pointer to the function bound to the visitor and node class.
Mark Linton coined the term "Visitor" in the X Consortium's Fresco Application
Toolkit specification [LP93]

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Interface Constraints**, **Delegation**

Behavioral patterns are concerned with algorithms and the assignment of responsibilities between objects.
Behavioral patterns describe not just patterns of objects or classes
but also the patterns of communication between them.

Behavioral class patterns use inheritance to distribute behavior between classes. This chapter includes two such
patterns.

Behavioral object patterns use object composition rather than inheritance.
Some describe how a group of peer objects cooperate to perform a task that no single object can carry out by itself.
An important issue here is how peer objects know about each other.
Peers could maintain explicit references to each other, but that would increase their coupling.
Some patterns provide indirection to allow loose coupling
mediator, chain of responsibility, observer
Other behavioral object patterns are concerned with encapsulating behavior in an object and delegating requests to it.
strategy, command, state, visitor, iterator

### Aspects that can vary

- Operations that can be applied to object(s) without changing their class(es).

### Solution to causes of redesign

- Algorithmic dependencies.
    - Algorithms are often extended, optimized, and replaced, forcing dependants to also change.

- Inability to alter classes conveniently.
    - e.g: classes from a commercial class library
    - or the change would require modifying many existing subclasses

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

Some of the benefits and liabilities of the Visitor pattern are asfollows:

1. Visitor makes adding newoperations easy.Visitorsmake it easy to add operations
   that depend on the components of complex objects. You can define a new
   operation over an object structure simply by adding a new visitor.In contrast,
   if you spread functionality over many classes, then you must change each
   class to define a new operation.
2. A visitor gathers related operations and separates unrelated ones. Related behavior isn't spread over the classes
   defining the object structure; it's localized
   in a visitor. Unrelated sets of behavior are partitioned in their own visitor subclasses. That simplifies both the
   classes defining the elements and the algorithms defined in the visitors. Any algorithm-specific data structures can
   be hidden in the visitor.
3. Adding new ConcreteElement classes is hard. The Visitor pattern makesit hard
   to add new subclasses of Element. Each new ConcreteElement gives rise to
   a new abstract operation on Visitor and a corresponding implementation
   in every ConcreteVisitor class. Sometimes a default implementation can be
   provided in Visitorthat can be inherited by most ofthe ConcreteVisitors, but
   this is the exception rather than the rule.
   So the key consideration in applying the Visitor pattern is whether you are
   mostly likely to change the algorithm applied over an object structure or
   the classes of objects that make up the structure. The Visitor class hierarchy
   can be difficult to maintain when new ConcreteElement classes are added
   frequently. In such cases, it's probably easier just to define operations on the
   classes that make up the structure. If the Element class hierarchy is stable,
   but you are continually adding operations or changing algorithms, then the
   Visitor pattern will help you manage the changes.
4. Visiting across class hierarchies. An iterator (see Iterator (257)) can visit the
   objects in a structure as it traverses them by calling their operations. But an
   iterator can't work across object structures with different types of elements.
   For example, the Iteratorinterface defined on page 263can access onlyobjects
   of type Item:
   template <class Item>
   class Iterator {
   // . . .
   Item Currentltem() const;
   };
   This implies that all elements the iterator can visit have a common parent
   class Item.
   Visitor does not have this restriction. It can visit objects that don't have a
   common parent class. You can add any type of object to a Visitor interface.
   For example, in
   class Visitor {
   public:
   // . . .
   void VisitMyType(MyType*);
   void VisitYourType(YourType*);
   };
   MyType and YourTypedo not have to be related through inheritance at all.
5. Accumulating state. Visitors can accumulate state as they visit each element
   in the object structure. Without a visitor, this state would be passed as extra
   arguments to the operations that performthe traversal, or they might appear
   as global variables
6. Breaking encapsulation. Visitor's approach assumes that the ConcreteElement
   interface is powerful enough to let visitors do their job.As a result, the pattern
   often forces you to provide public operations that access an element's internal
   state, which may compromise its encapsulation.

Open/Closed Principle. You can introduce a new behavior that can work with objects of different classes without changing
these classes.
Single Responsibility Principle. You can move multiple versions of the same behavior into the same class.
A visitor object can accumulate some useful information while working with various objects. This might be handy when you
want to traverse some complex object structure, such as an object tree, and apply the visitor to each object of this
structure.

You need to update all visitors each time a class gets added to or removed from the element hierarchy.
Visitors might lack the necessary access to the private fields and methods of the elements that they’re supposed to work
with.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

Composite (163):Visitorscan be used to apply an operation over an object structure
defined by the Composite pattern.
Interpreter (243): Visitor may be applied to do the interpretation.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

(recognizable by two different abstract/interface types which has methods defined which takes each the other
abstract/interface type; the one actually calls the method of the other and the other executes the desired strategy on
it)

### Structure

```mermaid
classDiagram
    class Visitor {
        <<interface>>
        + visitElementA(element: ElementA): void
        + visitElementB(element: ElementB): void
    }

    class ConcreteVisitor {
        + visitElementA(element: ElementA): void
        + visitElementB(element: ElementB): void
    }

    class Element {
        <<interface>>
        + accept(visitor: Visitor): void
    }

    class ConcreteElementA {
        + accept(visitor: Visitor): void
    }
    class ConcreteElementB {
        + accept(visitor: Visitor): void
    }

    Visitor <|.. ConcreteVisitor: implements
    Element <|.. ConcreteElementA: implements
    Element <|.. ConcreteElementB: implements
    Element --> Visitor: accepts
    ConcreteElementA <-- Visitor: visits
    ConcreteElementB <-- Visitor: visits

```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

Each object structure will have an associated Visitor class. This abstract visitor
class declares a VisitConcreteElement operation for each class of ConcreteEle-
ment defining the object structure. Each Visit operation on the Visitor declares
its argument to be a particular ConcreteElement, allowing the Visitor to access
the interface of the ConcreteElement directly. Concrete Visitor classes override
each Visit operation to implement visitor-specific behavior for the corresponding
ConcreteElement class.
The Visitor class would be declared like this in C++:

```text
class Visitor {
public:
virtual void VisitElementA(ElementA*);
virtual void VisitElementB(ElementB*);
// and so on for other concrete elements
protected:
Visitor();
};
```

Each class of ConcreteElement implements an Accept operation that calls the
matching Visit. . . operation on the visitor for that ConcreteElement. Thus the
operation that ends up getting called depends on both the class of the element
and the class of the visitor.10
The concrete elements are declared as

```text
class Element {
public:
virtual ~Element();
virtual void Accept(Visitors) = 0;
protected:
Element();
};
class ElementA : public Element {
public:
ElementA();
virtual void Accept(Visitor& v) { v.VisitElementA(this); }
};
class Elements : public Element {
public:
Elements();
virtual void Accept(Visitors v) { v.VisitElementB(this); }
}
```

A CompositeElement class might implement Accept like this:

```text
class CompositeElement : public Element {
public:
virtual void Accept(Visitor^);
private:
List<Element.*>* __children;
};
void CompositeElement::Accept (Visitork v) {
ListIterator<Element*> i(_children);
for (i. First (); 1 i . IsDone () ; i.NextO) {
i.Currentltem()->Accept(v);
}
v.VisitCompositeElement(this);
}
```

Here are two other implementation issues that arise when you apply the Visitor
pattern:

1. Double dispatch. Effectively, the Visitor pattern lets you add operations to
   classes without changing them. Visitor achieves this by using a technique
   called double-dispatch. It's a well-known technique. In fact, some program-
   ming languages support it directly (CLOS, for example). Languages like C++
   and Smalltalk support single-dispatch.
   In single-dispatch languages, two criteria determine which operation will
   fulfill a request: the name of the request and the type of receiver. For ex-
   ample, the operation that a GenerateCode request will call depends on the
   type of node object you ask. In C++, calling Generat eCode on an instance of
   VariableRef Node will call VariableRef Node: : GenerateCode (which
   generates code for a variable reference). Calling GenerateCode on an
   AssignmentNode will call AssignmentNode: : GenerateCode (which
   will generate code for an assignment). The operation that gets executed
   depends both on the kind of request and the type of the receiver.
   "Double-dispatch" simply means the operation that gets executed depends
   on the kind of request and the types of two receivers. Accept is a double-
   dispatch operation. Its meaning depends on two types: the Visitor's and the
2. Element's. Double-dispatching lets visitors request different operations on
   each class of element.11
   This is the key to the Visitor pattern: The operation that gets executed de-
   pends on both the type of Visitor and the type of Element it visits. Instead of
   binding operations statically into the Element interface, you can consolidate
   the operations in a Visitor and use Accept to do the binding at run-time. Ex-
   tending the Element interface amounts to defining one new Visitor subclass
   rather than many new Element subclasses.
2. Who is responsible for traversing the object structure? A visitor must visit each
   element of the object structure. The question is, how does it get there? We can
   put responsibility for traversal in any of three places: in the object structure,
   in the visitor, or in a separate iterator object (see Iterator (257)).
   Often the object structure is responsible for iteration. A collection will simply
   iterate over its elements, calling the Accept operation on each. A composite
   will commonly traverse itself by having each Accept operation traverse the
   element's children and call Accept on each of them recursively.
   Another solution is to use an iterator to visit the elements. In C++, you could
   use either an internal or external iterator, depending on what is available
   and what is most efficient. In Smalltalk, you usually use an internal iterator
   using do: and a block. Since internal iterators are implemented by the object
   structure, using an internal iterator is a lot like making the object structure
   responsible for iteration. The main difference is that an internal iterator will
   not cause double-dispatching—it will call an operation on the visitor with
   an element as an argument as opposed to calling an operation on the element
   with the visitor as an argument. But it's easy to use the Visitor pattern with
   an internal iterator if the operation on the visitor simply calls the operation
   on the element without recursing.
   You could even put the traversal algorithm in the visitor, although you'll end
   up duplicating the traversal code in each Concrete Visitor for each aggregate
   ConcreteElement. The main reason to put the traversal strategy in the visitor
   is to implement a particularly complex traversal, one that depends on the
   results of the operations on the object structure. We'll give an example of
   such a case in the Sample Code.

### Implementation

In the example we apply the visitor pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Visitor](https://refactoring.guru/design-patterns/visitor)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [Gui Ferreira: FINALLY, the Visitor Design Pattern makes Sense](https://youtu.be/yyKrt7zSmv0?si=lu7CYU7pqOI6iAkD)
- [Zoran Horvat: Visitor Design Pattern Is Giving Way To Pattern Matching Expressions!](https://youtu.be/tq5ztZO45-g?si=gN25Y3tFKoMI_rgO)
- [Geekific: The Visitor Pattern Explained and Implemented in Java](https://youtu.be/UQP5XqMqtqQ?si=42O6CwXMHznKkQjt)

<br>
<br>
